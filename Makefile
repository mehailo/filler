NAME = filler

SRC =	main.c \
        parsing.c \
        find_solution.c \
        fill_values.c \
        test.c

CFLAGS = -Wall -Wextra -Werror

CC = gcc

all:
	$(CC) $(CFLAGS) -Iinc -Llibft -lft $(SRC) -o $(NAME)

debug:
	$(CC) $(CFLAGS) -Iinc -Llibft -lft_debug $(SRC) -o $(NAME)

clean:
	rm $(NAME)