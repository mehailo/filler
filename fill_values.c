//
// Created by Mykhel Frankevych on 21.08.17.
//

#include "filler.h"

static void init_val_map(t_args *args)
{
	int i;
	int j;

	i = 0;
	args->map_vals = (int**)malloc((sizeof(int*) + 1) * args->map_height);
	args->map_vals[args->map_height] = NULL;
	while (i < args->map_height)
	{
		args->map_vals[i] = (int*)malloc((sizeof(int)) * args->map_width);
		j = 0;
		while (j < args->map_width)
		{
			if (args->map[i][j] == args->enemy_symb)
				args->map_vals[i][j] = 1;
			else
				args->map_vals[i][j] = 0;
			j++;
		}
		i++;
	}
}

static void fill_near_values(t_args *args, int i, int j)
{
	int cell_value;

	cell_value = args->map_vals[i][j];
	if (i - 1 >= 0 && args->map_vals[i - 1][j] == 0)
		args->map_vals[i - 1][j] = cell_value + 1;
	if (i + 1 < args->map_height && args->map_vals[i + 1][j] == 0)
		args->map_vals[i + 1][j] = cell_value + 1;
	if (j + 1 < args->map_width  && args->map_vals[i][j + 1] == 0)
		args->map_vals[i][j + 1] = cell_value + 1;
	if (j - 1 >= 0 && args->map_vals[i][j - 1] == 0)
		args->map_vals[i][j - 1] = cell_value + 1;
}

static void fill_value_map(t_args *args)
{
	int current_number;
	int i;
	int j;

	current_number = 1;
	while (current_number < args->map_height + args->map_height)
	{
		i = 0;
		while (i < args->map_height)
		{
			j = 0;
			while (j < args->map_width)
			{
				if (args->map_vals[i][j] == current_number)
					fill_near_values(args, i, j);
				j++;
			}
			i++;
		}
		current_number += 1;
	}
}

void choose_right_position(t_args *args)
{
	debug_output("check_figures");
	init_val_map(args);
	fill_value_map(args);
	find_best_position(args);
}
