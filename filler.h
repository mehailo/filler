//
// Created by Mykhailo Frankevich on 8/4/17.
//

#ifndef FILLER_H
# define FILLER_H

# include "libft/libft.h"
# include <fcntl.h>

typedef struct  s_coords
{
    int     x;
    int     y;
	int		sum;
}               t_coords;

typedef struct  s_args
{
    char        **map;
	int 		**map_vals;
    int         map_width;
    int         map_height;
    char        **figure;
    int         fig_width;
    int         fig_height;
    char 	    player_symb;
	char 	    enemy_symb;
    int         first_turn;
    t_list      *coords;
    t_coords    choosen_coords;

	int test_fd;
}               t_args;

/*
** Utilities
*/
void skip_lines(int count, t_args *args);
void debug_output(char *i_str);
void print_int_array(t_args *args);
void debug_print_coords(t_args *args);

void    read_from_stdin(t_args *args);
void    choose_right_position(t_args *args);
void	find_best_position(t_args *args);
void	get_line(int fd, char** line);


#endif
