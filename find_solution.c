//
// Created by Mykhel Frankevych on 21.08.17.
//

#include "filler.h"

static void find_sum_figure(t_args *args, t_list *node)
{
	int i;
	int j;
	t_coords *coords;

	coords = (t_coords*)node->content;
	coords->sum = 0;
	i = 0;
	while (i < args->fig_height)
	{
		j = 0;
		while (j < args->fig_width)
		{
			if (args->figure[i][j] == '*')
				coords->sum += args->map_vals[coords->y + i][coords->x + j];
			j++;
		}
		i++;
	}
}

void	find_best_position(t_args *args)
{
	t_list *crawler;

	crawler = args->coords;
	while (crawler)
	{
		find_sum_figure(args, crawler);
		crawler = crawler->next;
	}
	crawler = args->coords;
	args->choosen_coords.sum = MAX_INT;
	while(crawler)
	{
		if (((t_coords*)crawler->content)->sum < args->choosen_coords.sum)
			ft_memmove(&args->choosen_coords, crawler->content, sizeof(t_coords));
		crawler = crawler->next;
	}
	return ;
}
