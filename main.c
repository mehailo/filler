#include "filler.h"

int g_fin = 0;

void debug_output(char *i_str)
{
	if (i_str)
		;
//    ft_putstr_fd("     ", 2);
//    ft_putstr_fd(i_str, 2);
//    ft_putstr_fd("\n", 2);
}

int 	check_place_figure(t_args *args, int x, int y)
{
	int count;
	int i;
	int j;

	j = 0;
	count = 0;

	while (j < args->fig_height)
	{
		i = 0;
		while (i < args->fig_width)
		{
			if (args->figure[j][i] == '*')
			{
				if (args->map[y + j][x + i] == args->enemy_symb)
					return (0);
				if (args->map[y + j][x + i] == args->player_symb)
					count++;
				if (count > 1)
					return (0);
			}
			i++;
		}
		j++;
	}
	return (count == 1 ? 1 : 0);
}


void add_coord_to_lst(t_args *args, int x, int y)
{
	debug_output("add_coord_to_lst");
	t_coords coords;

	coords.x = x;
	coords.y = y;
	ft_lstpushback(&args->coords, ft_lstnew(&coords, sizeof(coords)));
//	debug_output(ft_itoa(y));
//	debug_output(ft_itoa(x));
//	debug_output("\n");

}

void get_possible_posittions(t_args *args)
{
	debug_output("get_possible_posittions");
    int x;
	int y;

	if (args->map == NULL || args->figure == NULL)
		return ;
	y = 0;
	while (y < args->map_height - args->fig_height)
	{
		x = 0;
		while (x < args->map_width - args->fig_width)
		{
			if (check_place_figure(args, x, y))
				add_coord_to_lst(args, x, y);
			x++;
		}
		y++;
	}
}

void place_figure(t_args *args)
{
	debug_output("place_figures");
	if (args)
		;
	ft_putnbr(args->choosen_coords.y);
	ft_putchar(' ');
	ft_putnbr(args->choosen_coords.x);
	ft_putchar('\n');
}

void skip_lines(int count, t_args *args)
{
	debug_output("skip_lines");
    char *line;

    while (count)
    {
        get_line(args->test_fd, &line);
		if (ft_strlen(line) > 0)
        	free(line);
        count--;
    }
    return ;
}

void main_cycle(t_args *args)
{
	debug_output("main_cycle");
    while (42)
    {
		args->coords = NULL;
        read_from_stdin(args);
        get_possible_posittions(args);
        choose_right_position(args);

//		debug_print_coords(args);
//		print_int_array(args);

        place_figure(args);
        if (g_fin)
            break;
    }
}

int main(void)
{
    t_args args;

	ft_bzero(&args, sizeof(args));
    args.test_fd = 0;
    //args.test_fd = open("test_input", O_RDONLY);
    args.first_turn = 1;
    main_cycle(&args);
    return 0;
}

//          ./filler_vm -f maps/map01 -p1 players/abanlin.filler -p2 ../a.out