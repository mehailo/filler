//
// Created by Mykhel Frankevych on 20.08.17.
//
#include "filler.h"

void	get_line(int fd, char** line)
{
	while(get_next_line(fd, line) <= 0 || ft_strlen(*line) == 0)
		;
}

static void read_figure(t_args *args)
{
	char *line;
	int i;

	i = 0;
	line = NULL;
	//while (line == NULL || ft_strlen(line) == 0)
		get_line(args->test_fd, &line);
	args->fig_height = ft_atoi(ft_strchr(line, ' ') + 1);
	args->fig_width = ft_atoi(ft_strchr(ft_strchr(line, ' ') + 1, ' ') + 1);
	args->figure = (char**)malloc(sizeof(char*) * args->fig_height + 1);
	args->figure[args->fig_height] = NULL;
	while (i < args->fig_height)
	{
		get_line(args->test_fd, &line);
		args->figure[i] = ft_strdup(line);
		free(line);
		i++;
	}
}


static void read_map(t_args *args)
{
	char *line;
	int i;

	i = 0;
	args->map = (char**)malloc(sizeof(char*) * args->map_height + 1);
	args->map[args->map_height] = NULL;
	while(i < args->map_height)
	{
		get_line(args->test_fd, &line);
		args->map[i] = ft_strdup(&line[4]);
		free(line);
		i++;
	}
}


static void start_info(t_args *args)
{
	char *line = "";

	get_line(args->test_fd, &line);
	if (line[10] == '1')
		args->player_symb = 'O';
	else
		args->player_symb = 'X';
	args->enemy_symb = args->player_symb == 'O' ? 'X' : 'O';
	free(line);
	get_line(args->test_fd, &line);
	args->map_height = ft_atoi(ft_strchr(line, ' ') + 1);
	args->map_width = ft_atoi(ft_strchr(ft_strchr(line, ' ') + 1, ' ') + 1);
	free(line);
	get_line(args->test_fd, &line);
	free(line);
	args->first_turn = 0;
}

void read_from_stdin(t_args *args)
{
	char *line;

	line = NULL;
	if (args->first_turn)
		start_info(args);
	else
		skip_lines(2, args);
	read_map(args);
	read_figure(args);
}