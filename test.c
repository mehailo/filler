//
// Created by Mykhel Frankevych on 21.08.17.
//

#include "filler.h"

void print_int_array(t_args *args)
{
	int i;
	int j;

	i = 0;
	while (i < args->map_height)
	{
		ft_putstr_fd("    ",2);
		j = 0;
		while (j < args->map_width)
		{
			ft_putnbr_fd(args->map_vals[i][j], 2);
			ft_putchar_fd(' ', 2);
			j++;
		}
		ft_putchar_fd('\n', 2);
		i++;
	}
}

void debug_print_coords(t_args *args)
{
	t_list *crawler;
	t_coords *coords;

	crawler = args->coords;
	while (crawler)
	{
		coords = crawler->content;
		ft_putstr_fd("    y ", 2);
		ft_putnbr_fd(coords->y, 2);
		ft_putstr_fd("  x ", 2);
		ft_putnbr_fd(coords->x, 2);
		ft_putstr_fd("  sum ", 2);
		ft_putnbr_fd(coords->sum, 2);
		ft_putchar_fd('\n', 2);
		crawler = crawler->next;
	}
}
